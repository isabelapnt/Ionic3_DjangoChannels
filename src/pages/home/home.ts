
declare const ReconnectingWebSocket: any;

import {config} from "../../providers/config"
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// import { WebSocket } from '../../providers/websocket';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	data: Array<string> ;
	task: {name?: string} = {};
	ws_scheme : any;
    ws_path : any;
    socket : any;
	constructor(public navCtrl: NavController) {
		this.data = []
  		this.ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
	    this.ws_path = this.ws_scheme + '://' + config.API.host + '/dashboard/';
	    this.socket = new ReconnectingWebSocket(this.ws_path);
	}	
    send(taskForm){
	   	let message = {
	        action: "start_sec3",
	        job_name: this.task.name,
	    };
	   	this.socket.send(JSON.stringify(message))
	  	this.recive()
    }

    recive(){
    	let self = this
   		this.socket.onmessage = function(message) {
	        console.log("Got message: " + message.data);
	        self.data.push(message.data)
    	};
    }
}
